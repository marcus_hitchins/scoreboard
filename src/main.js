import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

import { firestorePlugin } from 'vuefire'
import vuetify from './plugins/vuetify';

Vue.use(firestorePlugin)

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')

import firebase from 'firebase'

firebase.initializeApp({
  "apiKey": "AIzaSyAU8KvY4SPAz4q9hN8puugGZmShe-7aBBk",
  "databaseURL": "https://tnmoc-app.firebaseio.com",
  "storageBucket": "tnmoc-app.appspot.com",
  "authDomain": "tnmoc-app.firebaseapp.com",
  "messagingSenderId": "631934032825",
  "projectId": "tnmoc-app"
});

export const db = firebase.firestore()
